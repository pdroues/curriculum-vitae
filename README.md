# Curriculum vitæ

C'est un dépot git pour versionner mon CV fait avec [RenderCV](https://app.rendercv.com/)

```bash
python3 -m venv .venv
source .venv/bin/activate
pip install requirement.txt
mkdir -p rendercv_output
mv face.jpg rendercv_output/profile_picture.jpg
rendercv render "Philemon_Droues_CV.yml"
```
